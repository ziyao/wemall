<?php
return array(
	//'配置项'=>'配置值'


	/*
	 * 0:普通模式 (采用传统癿URL参数模式 )
	 * 1:PATHINFO模式(http://<serverName>/appName/module/action/id/1/)
	 * 2:REWRITE模式(PATHINFO模式基础上隐藏index.php)
	 * 3:兼容模式(普通模式和PATHINFO模式, 可以支持任何的运行环境, 如果你的环境不支持PATHINFO 请设置为3)
	 */
	'URL_MODEL'=>3,
	
	//Auth权限设置
	'AUTH_CONFIG' => array(
	    'AUTH_ON' => true,  // 认证开关
	    'AUTH_TYPE' => 1, // 认证方式，1为实时认证；2为登录认证。
	    'AUTH_GROUP' => 'wemall_auth_group', // 用户组数据表名
	    'AUTH_GROUP_ACCESS' => 'wemall_auth_group_access', // 用户-用户组关系表
	    'AUTH_RULE' => 'wemall_auth_rule', // 权限规则表
	    'AUTH_USER' => 'wemall_member', // 用户信息表
	),   
	'SESSION_AUTO_START'=>true,
	'DWZ_VERSION' => '1.4.6', //DWZ版本
	'PAGE_LISTROWS' => 15, //每页显示的记录数，初始显示
	'VAR_PAGE' => 'pageNum',
	'DB_LIKE_FIELDS'=>'username|email|qq|title|goods_name', //支持模糊查询的字段
	'NOT_M_MODULE' => 'Index,Exit', //无需执行实例化的模块

	'AUTH_SUPERADMIN' => '1,2', //Auth权限认证超级管理员


);