<?php
$comconfig =  array(
		//官方配置
		'WECHART_CONFIG' => array(
 			'token'=>'', //填写你设定的key
 			'appid'=>'', //填写高级调用功能的app id
 			'appsecret'=>'', //填写高级调用功能的密钥
  			'partnerid'=>'', //财付通商户身份标识
 			'partnerkey'=>'', //财付通商户权限密钥Key
 			'paysignkey'=>'' //商户签名密钥Key
		),

		//非官方配置
		'WECHATTEXT_CONFIG' => array(
			'account'=>'',
	        'password'=>'',
	        'datapath'=>'',
            'debug'=>true,
            'logcallback'=>''
        ),
	
);

$dbconfig = require CONF_PATH.'dbconfig.php';//数据库配置
return array_merge($comconfig,$dbconfig);
?>