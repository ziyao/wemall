/*
Navicat MySQL Data Transfer

Source Server         : project001
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : wemall

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2014-10-11 18:23:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `wemall_admin`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_admin`;
CREATE TABLE `wemall_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_admin
-- ----------------------------
INSERT INTO `wemall_admin` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2014-10-10 13:54:13');

-- ----------------------------
-- Table structure for `wemall_auth_extend`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_auth_extend`;
CREATE TABLE `wemall_auth_extend` (
  `group_id` mediumint(10) unsigned NOT NULL COMMENT '用户id',
  `extend_id` mediumint(8) unsigned NOT NULL COMMENT '扩展表中数据的id',
  `type` tinyint(1) unsigned NOT NULL COMMENT '扩展类型标识 1:栏目分类权限;2:模型权限',
  UNIQUE KEY `group_extend_type` (`group_id`,`extend_id`,`type`),
  KEY `uid` (`group_id`),
  KEY `group_id` (`extend_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户组与分类的对应关系表';

-- ----------------------------
-- Records of wemall_auth_extend
-- ----------------------------
INSERT INTO `wemall_auth_extend` VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for `wemall_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_auth_group`;
CREATE TABLE `wemall_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组id,自增主键',
  `module` varchar(20) NOT NULL COMMENT '用户组所属模块',
  `type` tinyint(4) NOT NULL COMMENT '组类型',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `description` varchar(80) NOT NULL DEFAULT '' COMMENT '描述信息',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户组状态：为1正常，为0禁用,-1为删除',
  `rules` varchar(500) NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id，多个规则 , 隔开',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_auth_group
-- ----------------------------
INSERT INTO `wemall_auth_group` VALUES ('1', 'admin', '1', '默认用户组', '默认用户组', '1', '1,2,3,4,5,6,');

-- ----------------------------
-- Table structure for `wemall_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_auth_group_access`;
CREATE TABLE `wemall_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '用户id',
  `group_id` mediumint(8) unsigned NOT NULL COMMENT '用户组id',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_auth_group_access
-- ----------------------------
INSERT INTO `wemall_auth_group_access` VALUES ('3', '1');

-- ----------------------------
-- Table structure for `wemall_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_auth_rule`;
CREATE TABLE `wemall_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `module` varchar(20) NOT NULL COMMENT '规则所属module',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1-url;2-主菜单',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`module`,`name`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_auth_rule
-- ----------------------------
INSERT INTO `wemall_auth_rule` VALUES ('1', 'admin', '1', 'Admin/Index/index', '后台首页', '1', '');
INSERT INTO `wemall_auth_rule` VALUES ('2', 'admin', '1', 'Auth/Index/index', '管理首页', '1', '');

-- ----------------------------
-- Table structure for `wemall_goods`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_goods`;
CREATE TABLE `wemall_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `title` text NOT NULL,
  `price` text NOT NULL,
  `old_price` text NOT NULL,
  `image` text NOT NULL,
  `detail` text NOT NULL,
  `status` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_goods
-- ----------------------------

-- ----------------------------
-- Table structure for `wemall_info`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_info`;
CREATE TABLE `wemall_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `gonggao` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_info
-- ----------------------------
INSERT INTO `wemall_info` VALUES ('1', 'wemall', '欢迎来到wemall');

-- ----------------------------
-- Table structure for `wemall_mail`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_mail`;
CREATE TABLE `wemall_mail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `smtp` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `on` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_mail
-- ----------------------------

-- ----------------------------
-- Table structure for `wemall_menu`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_menu`;
CREATE TABLE `wemall_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` text NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `wemall_orders`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_orders`;
CREATE TABLE `wemall_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `orderid` text NOT NULL,
  `totalprice` text NOT NULL,
  `pay_status` text NOT NULL,
  `note` text NOT NULL,
  `orders_status` text NOT NULL,
  `print_status` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_orders
-- ----------------------------

-- ----------------------------
-- Table structure for `wemall_ordersinfo`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_ordersinfo`;
CREATE TABLE `wemall_ordersinfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `price` text NOT NULL,
  `num` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_ordersinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `wemall_theme`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_theme`;
CREATE TABLE `wemall_theme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `theme` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_theme
-- ----------------------------
INSERT INTO `wemall_theme` VALUES ('1', 'default');

-- ----------------------------
-- Table structure for `wemall_users`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_users`;
CREATE TABLE `wemall_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_users
-- ----------------------------
INSERT INTO `wemall_users` VALUES ('1', '子遥', '123456', '2014-10-11 11:01:27');

-- ----------------------------
-- Table structure for `wemall_weixin`
-- ----------------------------
DROP TABLE IF EXISTS `wemall_weixin`;
CREATE TABLE `wemall_weixin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `picurl` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wemall_weixin
-- ----------------------------
INSERT INTO `wemall_weixin` VALUES ('1', '欢迎来到微信商城', '欢迎来到微信商城，关注微信号:iwemall', '538d80973f66e.jpg');
